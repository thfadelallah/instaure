import React from "react";
import  { Header } from "../home/header";
import img5 from "../../assets/img/img5.png";
import style from "./style.module.css";
import  PhoneIphoneIcon  from "@material-ui/icons/PhoneIphone";
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import { TextField, TextareaAutosize ,Button }  from '@material-ui/core';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import InstagramIcon from '@material-ui/icons/Instagram';
import { Footer } from "../home/footer"




export const contact= (props) => {
  return (
    <div >
    <Header  history={props?.history} />
    
    <img
    src={img5}
    className={style.img}
    />
    <p className={style.text}>
      contact us
    </p>

    <p className={style.get}>Get in touch with us ! </p>
    <div className={style.view}>
    <div className={style.part}>
    < PhoneIphoneIcon 
    fontSize="large"
    style={{
      marginLeft:"43%",
      color:"#286CA5"
    }} />
    <p className={style.phone}> phone </p>
    <p className={style.num}> +20 111 111 111 <br /> +20 222 222 222 </p>
    </div>
    <div className={style.part2}>
    < LocationOnOutlinedIcon
    fontSize="large"
    style={{
      marginLeft:"43%",
      color:"#286CA5"
    }} />
    <p className={style.phone}> address</p>
    <p className={style.num}> 48 Mohi Al Din Abou Al Ezz,  <br />  Ad Doqi ,Dokki, Giza Governorate </p>
    </div>
    <div className={style.part3}>
    < EmailOutlinedIcon 
    fontSize="large"
    style={{
      marginLeft:"43%",
      color:"#286CA5"
    }} />
    <p className={style.phone}> Email </p>
    <p className={style.num}> email@gmail.com <br /> email@gmail.com </p>
    </div>
    </div>
    <p className={style.IF}>  If you have any question </p>
    <div className={style.form}>
      <TextField
          label="Your Name"
          type="name"
          variant="outlined"
          style={{marginTop:"20px" ,width:"80%", marginLeft:"10%"}}
     />
     <TextField
          label="Email"
          type="email"
          variant="outlined"
          style={{marginTop:"20px" ,width:"80%", marginLeft:"10%", height:"30%"}}
      />
    
      <TextareaAutosize
      aria-label="minimum height"
      minRows={7}
      placeholder="Message"
      style={{ width:'880px', marginLeft:"10%", marginTop:"20px" ,fontSize:'15px', backgroundColor:"#F1F1F1", borderColor:"#A8A8A8"}}
    />
    <Button 
      style={{
        marginLeft:"20%",
        marginTop:"20px",
        backgroundColor:"#286CA5",
        width: "50%",
        color:"#ffffff",
        marginBottom: "5%"
      }}
      variant="contained"
      >
        send
      </Button>
    </div>   
     <p className={style.IFF}>  Connect with us </p> 
     <div >
     <FacebookIcon 
     fontSize="large"
     style={{
      marginLeft:"42%",
      color:"#286CA5"
    }}
    />
     <LinkedInIcon 
     fontSize="large"
     style={{
      marginLeft:"3%",
      color:"#286CA5"
    }} />
     <TwitterIcon
     fontSize="large"
     style={{
      marginLeft:"3%",
      color:"#286CA5"
    }} />
     <InstagramIcon 
     fontSize="large"
     style={{
      marginLeft:"3%",
      color:"#286CA5"
    }}/>
    </div>
     <Footer history={props?.history} />
    </div>
    
  );
};
