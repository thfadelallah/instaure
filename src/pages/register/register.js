import React, { useState, useEffect } from "react";
import img1 from "../../assets/img/img1.png";
import style from "./style.module.css";
import { TextField, Button } from "@material-ui/core";
import { BasicSelect } from "../component/select";
import { postService } from "../services/axios";
import { getItem } from "../services/storage";

export const Register = (props) => {
  const [dataa, setData] = useState({
    name: "",
    age: "",
    email: "",
    pass: "",
    login: false,
  });
  const [errorMessage, seterrorMessage] = useState(false);

  useEffect(async () => {
    if (await getItem?.("userData")?.token) {
      props?.history?.push?.("home");
    }
  }, []);

  const sendRequest = () => {
    postService("SignUp", dataa)
      .then((res) => {
        //setItem("userData", res?.data?.data);
        console.log("res", res?.data);
        props?.history?.push?.("login");
      })
      .catch((error) => {
        console.log("error", error?.response?.data?.error);
        seterrorMessage(error?.response?.data?.error);
      });

    console.log(dataa);
    // props?.history?.push?.("login")
  };

  return (
    <div>
      <img
        src={img1}
        style={{
          width: "40%",
        }}
      />
      <h1 className={style.text}>sign up </h1>

      <div className={style.input}>
        <TextField
          label="name"
          type="name"
          variant="outlined"
          onChange={(name) => {
            // console.log(firstName.target.value);
            setData({ ...dataa, name: name?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />
        {/* <TextField
          label="Last Name"
          type="name"
          variant="outlined"
          onChange={(lastName) => {
            setData({ ...dataa, lastName: lastName?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        /> */}
        <TextField
          label="Email"
          type="email"
          variant="outlined"
          onChange={(email) => {
            setData({ ...dataa, email: email?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />
        <TextField
          label="Password"
          type="password"
          autoComplete="current-password"
          variant="outlined"
          onChange={(pass) => {
            setData({ ...dataa, pass: pass?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />
        <TextField
          label="age"
          type="age"
          variant="outlined"
          onChange={(age) => {
            setData({ ...dataa, age: age?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />
        {/* <TextField
          label="Phone"
          // type="number"
          variant="outlined"
          onChange={(phone) => {
            setData({ ...dataa, phone: phone?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />
        <TextField
          label="Adress"
          type="adress"
          variant="outlined"
          onChange={(address) => {
            setData({ ...dataa, address: address?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        /> */}
        {errorMessage ? (
          <h1 className={style.textInvalid}>{errorMessage}</h1>
        ) : null}
      </div>
      {/* <BasicSelect /> */}

      <Button
        style={{
          marginLeft: "50%",
          marginTop: "20px",
          backgroundColor: "#286CA5",
          width: "30%",
          color: "#ffffff",
          marginBottom: "5%",
        }}
        variant="contained"
        onClick={() => sendRequest()}
      >
        sign up
      </Button>
    </div>
  );
};
