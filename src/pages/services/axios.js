import axios from "axios";
import { getItem } from "./storage";

export const postService = async (url, requestData) => {
  //const userData = await getItem("userData");
  //console.log("ddd", userData?.token);
  return axios.post(`http://18.204.247.170:10000/${url}`, requestData, {
   //headers: { Authorization: userData?.token },
  });
};

export const getService = async (url, requestData) => {
  //const userData = await getItem("userData");
  //console.log("ddd", userData?.token);
  return axios.get(
    `http://18.204.247.170:10000/${url}`,
    //{ headers: { Authorization: userData?.token } },
    requestData
  );
};

