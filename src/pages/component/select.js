import  React, { useState } from 'react';
import { TextField, MenuItem } from "@material-ui/core";
import style from "../register/style.module.css"




export function BasicSelect() {

  const [dataa, setData] = useState({
    userType: "",
  });

    return (
        <div className={style.input}>
          
         <TextField
          id="outlined-select-type"
          select
          label="Type"
          variant="outlined"
          onChange={(userType) => {
            setData({ ...dataa, userType});
          }}
        //   helperText="Please select your type"
          style={{marginTop:"20px" ,width:"70%"}}
        >
         <MenuItem value={1}> patient</MenuItem>
         <MenuItem value={2}> doctor</MenuItem>
        </TextField>
        </div>
    );
}