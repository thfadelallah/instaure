import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {getService} from "../services/axios";
import {postService} from "../services/axios";
import {Button} from "@material-ui/core";
import { Header } from "../home/header"

function createData(ID, PatientName, HospitalName, Address, Price, Status, Description) {
  return { ID, PatientName, HospitalName, Address, Price, Status,Description };
}


export const  PindingTable=(props)=> {
 const [dataa, setDataa] = useState([]);
 
 


  const onClickApproved = (ID) => {
    const body={
        id:String(ID),
        Status: "approved"}
     console.log(body);
    postService("Changestatus",body)
      .then((res) => {
        console.log("res", res?.data);
      })
      .catch((error) => {
        console.log("error", error?.response?.data);
      });
  };
  const onClickReject = (ID) => {
    const body={
        id:String(ID),
        Status: "reject"}
     console.log(body);
    postService("Changestatus",body)
      .then((res) => {
        console.log("res", res?.data);
      })
      .catch((error) => {
        console.log("error", error?.response?.data);
      });
  };



  return (
    <div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align="right">Patient Name</TableCell>
            <TableCell align="right">Hospital Name</TableCell>
            <TableCell align="right">Address</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Status</TableCell>
            <TableCell align="right">Description</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {props?.data?.map?.((row) => {
              return(
                <TableRow
                key={row?.ID}
                // sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell >
                  {row?.ID}
                </TableCell>
                <TableCell align="right">{row?.PatientName}</TableCell>
                <TableCell align="right">{row?.HospitalName}</TableCell>
                <TableCell align="right">{row?.Address}</TableCell>
                <TableCell align="right">{row?.Price}</TableCell>
                <TableCell align="right">{row?.Status}</TableCell>
                <TableCell align="right">{row?.Description}</TableCell>    

              {row?.Record?.Status== "reject" ? <h1 
              style={{
              color:"#FFFF", 
              fontSize:"18px" ,
              textAlign:"center",
              backgroundColor:"red",
              borderRadius:"10px",
              marginRight:"40px",
              marginLeft:"40px",
              paddingTop:"5px",
              paddingBottom:"5px"
            }}
              >Reject</h1> 
              
              :
               row?.Record?.Status=="approved" ? <h1 
               style={{
                 color:"#ffff",
                 fontSize:"18px",
                 textAlign:"center",
                 backgroundColor:"green",
                 borderRadius:"10px",
                 marginRight:"40px",
                 marginLeft:"40px",
                 paddingTop:"5px",
                 paddingBottom:"5px"
               }}
            >Approved</h1> : 
              <div style={{
                      display:"flex",
                      flexDirection:"row",
                      justifyContent:"space-between",
                      marginLeft:"5%",
                      marginRight:"5%"
                    }}>
                     <Button 
                            style={{
                                backgroundColor:"#1B5E20",
                                color:"#fff",
                                marginLeft:"5%"
                            }}
                            onClick={ () => onClickApproved (row?.Record?.ID)  }
                            variant="contained" >
                              approved
                           </Button>
                           <Button 
                           style={{
                               color:"#D52F2F",
                               borderColor:"#D52F2F",
                               marginLeft:"20px"
                           }}
                           onClick={ () => onClickReject (row?.Record?.ID)  }
                           variant="outlined" >
                             reject
                           </Button>
                    </div>  
                  }     
            </TableRow>

            )
            }
          )}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  );
}