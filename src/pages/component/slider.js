import React, { useState, useEffect } from "react";
import profile from "../../assets/img/profile.png";
import style from "../home/style.module.css";
import homeIcon2 from "../../assets/img/homeIcon2.png";
import user from "../../assets/img/user.png";
import { getItem } from "../services/storage";
import { useHistory } from "react-router";

   



export const Slider = (props) => {
    const [User, setUser] = useState("");
    
    useEffect(async (res) => {
     setUser(getItem("userData"));
      console.log('ay7aga',await getItem("userData"));
    }, []);
  

  return (
    
    <div className={style.profile}>
       <img
         src={profile}
         width="100"
         height="100"
         className={style.img}
       />
         
       <p className={style.pp}>{User?.name}</p>
       <p className={style.m}>{User?.email}</p>
       <div 
         className={style.dash}
         onClick={() => props?.history?.push?.("home")}
        >
           <img
            src={homeIcon2}
            width="35"
            height="35"
           />
           <p style={{fontSize:"20px" , textTransform:"capitalize"}}>dashboard</p>
       </div>
       <div 
         className={style.dashhh}
         onClick={() => props?.history?.push?.("userHome")}
        >
           <img
            src={homeIcon2}
            width="35"
            height="35"
           />
           <p style={{fontSize:"20px" , textTransform:"capitalize"}}>Patient dashboard</p>
       </div>
       <div 
        className={style.dashh}
       onClick={() => props?.history?.push?.("account")}
        >
           <img
            src={user}
            width="30"
            height="25"
           />
           <p style={{fontSize:"20px" , textTransform:"capitalize"}}>account</p>
       </div>
       <div 
        className={style.dashh}
       onClick={() => props?.history?.push?.("AddPrice")}
        >
          <p style={{fontSize:"20px" , textTransform:"capitalize"}}>add Price</p>
       </div>
    </div>
   
   
   
  );
};
