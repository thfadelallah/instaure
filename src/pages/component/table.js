import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {postService} from "../services/axios";
import TablePagination from '@mui/material/TablePagination';
import { getItem } from "../services/storage";


function createData(ID, PatientName, HospitalName, Address, Price, Status, Description) {
  return { ID, PatientName, HospitalName, Address, Price, Status,Description };
}



export const  UserTable=(props)=> {
  

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };


  return (
    <Paper sx={{ width: '100%' }}>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align="right">Patient Name</TableCell>
            <TableCell align="right">Hospital Name</TableCell>
            <TableCell align="right">Address</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Status</TableCell>
            <TableCell align="right">Description</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          
          {props?.data?.slice?.(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row) => {
              return(
                <TableRow
                key={row?.ID}
                // sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell >
                  {row?.ID}
                </TableCell>
                <TableCell align="right">{row?.PatientName}</TableCell>
                <TableCell align="right">{row?.HospitalName}</TableCell>
                <TableCell align="right">{row?.Address}</TableCell>
                <TableCell align="right">{row?.Price}</TableCell>
                <TableCell align="right">{row?.Status}</TableCell>
                <TableCell align="right">{row?.Description}</TableCell>
              </TableRow>
              )
            } 
           
          )}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
        rowsPerPageOptions={[5, 10,25, 100]}
        component="div"
        count={props?.data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}