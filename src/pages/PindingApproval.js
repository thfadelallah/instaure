import React, { useState, useEffect } from "react";
import {getService} from "./services/axios";
import {postService} from "./services/axios";
import { Header } from "./home/header"
import { PindingTable } from "./component/pindingTable";
import { styled, alpha } from '@mui/material';
import SearchIcon from '@material-ui/icons/Search';
import { InputBase  } from '@mui/material';

function createData(ID, PatientName, HospitalName, Address, Price, Status, Description) {
  return { ID, PatientName, HospitalName, Address, Price, Status,Description };
}


const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));
const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));
const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

export const  PindingApproval=(props)=> {
  const [dataa, setData] = useState({
    ID:""
  });
  const [errorMessage, seterrorMessage] = useState(false);
  
  const [AllDataa, setAllDataa] = useState([]);
 

  useEffect(() => {
    getService("GetAllpindingApproval", [])
      .then((res) => {
        console.log("res", res?.data?.pindingApprovals);
         setAllDataa(res?.data?.pindingApprovals);
      })
      .catch((error) => {
        console.log("error", error?.response?.data);
      });
  }, []);


  const GetApprovalbyID = () => {
    seterrorMessage(false);
    postService("GetApprovalbyID", dataa)
      .then((res) => {
        console.log("res", res?.data?.message?.ID);
        setAllDataa([res?.data?.message]);
      })
      .catch((error) => {
        console.log(error?.response?.data?.[0]);
         seterrorMessage("ID is not found");
      });
    console.log(dataa);

  };




  return (
    <div>
    <Header  history={props?.history}/>
    <div style={{
      display: 'flex',
      flexDirection:"row",
      justifyContent:"space-between",
    }}>
    <p style={{
                textTransform:"capitalize",
                fontWeight:"bold",
                fontSize:"30px",
                marginLeft:"10%",
                // textAlign:"center",
                color:" #286CA5",
            }}>Pinding All Approval</p>
            {/* <div style={{
                 flexDirection: 'column',
                 display: 'flex',
                 marginLeft: '25%',
            }}></div> */}

            
            <Search style={{
              margin: "3%",
              backgroundColor: "#F5F5F5",
              marginRight:"10%",
          }}>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            
           
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
              onChange={(ID) => {
                setData({ ...dataa, ID: ID?.target.value });
               }}
            />  
                  
           <button
           style={{
            color:"#fff",
            backgroundColor:"#286CA5",
            fontSize:"large",
           marginRight:"10px"
           }}
           onClick={GetApprovalbyID}
           >Search</button>
         
          </Search>
            </div>
          

           <PindingTable data={AllDataa}/> 

   
    </div>
  );
}