
import React, { useState  } from "react";
import { Header } from './home/header';
import {TextField, Button} from "@material-ui/core";
import { postService } from "./services/axios";
import { style } from "./register/style.module.css"



export const CreateApproval = (props) => {
    const [dataa, setData] = useState({
        ID:"",
        PatientName: "",
        HospitalName: "",
        Address: "",
        Price: "",
        Status: "",
        Description:""
      });
      const [errorMessage, seterrorMessage] = useState(false);
    
      const CreateAproval = () => {
        seterrorMessage(false);
        postService("CreateAproval", dataa)
          .then((res) => {
            console.log("res", res?.data?.data);
            seterrorMessage("Success ,your Approval created successfully");
          })
          .catch((error) => {
            console.log(error?.response?.data?.[0]);
            seterrorMessage("Invalid data");
            // seterrorMessage(error?.response?.data.message);
          });
        console.log(dataa);
      };
    return (
        <div >
           <Header  history={props?.history}/>
           <p style={{
                textTransform:"capitalize",
                fontWeight:"bold",
                fontSize:"30px",
                textAlign:"center",
                color:" #286CA5"
            }}>Create a new approval</p>
            <div style={{
                 flexDirection: 'column',
                 display: 'flex',
                 marginLeft: '25%',
            }}>
            <TextField
              label="ID"
              type="ID"
              variant="outlined"
              onChange={(ID) => {
                 setData({ ...dataa, ID: ID?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
            <TextField
              label="Patient  Name"
              type="PatientName"
              variant="outlined"
              onChange={(PatientName) => {
                 setData({ ...dataa, PatientName: PatientName?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
            <TextField
              label="Hospital Name"
              type="HospitalName"
              variant="outlined"
              onChange={(HospitalName) => {
                 setData({ ...dataa, HospitalName: HospitalName?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
             <TextField
              label="Address"
              type="Address"
              variant="outlined"
              onChange={(Address) => {
                 setData({ ...dataa, Address: Address?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
            <TextField
              label="Price"
              type="Price"
              variant="outlined"
              onChange={(Price) => {
                 setData({ ...dataa, Price: Price?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
            <TextField
              label="Status"
              type="Status"
              variant="outlined"
              onChange={(Status) => {
                 setData({ ...dataa, Status: Status?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
             <TextField
              label="Description"
              type="Description"
              variant="outlined"
              onChange={(Description) => {
                 setData({ ...dataa, Description: Description?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
      
            </div>
            {errorMessage && (
          <h2
            style={{
              color: errorMessage.includes("success") ? "green" : "red",
            }}
            // className={style.errorMessage}
            style={{
                margin: "0%",
                padding: "0%",
                fontSize: "15px",
                marginTop:"5px",
                // colore:"red",
                marginLeft:"25%"
            }}
          >
            {errorMessage}
          </h2>
        )}
            <Button
             style={{
                marginLeft: "37%",
                marginTop: "20px",
                backgroundColor: "#286CA5",
                width: "30%",
                color: "#ffffff",
                marginBottom: "5%",
               }}
              variant="contained"
              onClick={() => CreateAproval()}
            >
            create approval
           </Button>   
        </div>
    );
}