
import React, { useState  } from "react";
import { Header } from './home/header';
import {TextField, Button} from "@material-ui/core";
import { postService } from "./services/axios";




export const AddPrice = (props) => {
    const [dataa, setData] = useState({
        ID:"",
        Price: ""
      });
      const [errorMessage, seterrorMessage] = useState(false);
    
      const AddPrice = () => {
        seterrorMessage(false);
        postService("Addprice", dataa)
          .then((res) => {
            console.log("res", res?.data);
            seterrorMessage("Success ,add price");
          })
          .catch((error) => {
            console.log(error?.response?.data?.[0]);
            seterrorMessage("Invalid data");
            // seterrorMessage(error?.response?.data.message);
          });
        console.log(dataa);
      };
    return (
        <div >
           <Header  history={props?.history}/>
           <p style={{
                textTransform:"capitalize",
                fontWeight:"bold",
                fontSize:"30px",
                textAlign:"center",
                color:" #286CA5"
            }}>Add price</p>
            <div style={{
                 flexDirection: 'column',
                 display: 'flex',
                 marginLeft: '25%',
            }}>
            <TextField
              label="ID"
              type="ID"
              variant="outlined"
              onChange={(ID) => {
                 setData({ ...dataa, ID: ID?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
           
            <TextField
              label="Price"
              type="Price"
              variant="outlined"
              onChange={(Price) => {
                 setData({ ...dataa, Price: Price?.target.value });
                }}
              style={{ marginTop: "20px", width: "70%" }}
            />
           
            </div>
            {errorMessage && (
          <h2
            style={{
              color: errorMessage.includes("success") ? "green" : "red",
            }}
            // className={style.errorMessage}
            style={{
                margin: "0%",
                padding: "0%",
                fontSize: "15px",
                marginTop:"5px",
                // colore:"red",
                marginLeft:"25%"
            }}
          >
            {errorMessage}
          </h2>
        )}
            <Button
             style={{
                marginLeft: "37%",
                marginTop: "20px",
                backgroundColor: "#286CA5",
                width: "30%",
                color: "#ffffff",
                marginBottom: "5%",
               }}
              variant="contained"
              onClick={() => AddPrice()}
            >
           add price
           </Button>   
        </div>
    );
}