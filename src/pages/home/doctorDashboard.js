
import React, { useState ,useEffect } from "react";
import Table from "../component/table";
import { DataTable } from "../component/dataTable";
import { styled, alpha } from '@mui/material';
import SearchIcon from '@material-ui/icons/Search';
import { InputBase  } from '@mui/material';
import { postService, getService } from "../services/axios";

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  }));
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
  }));




export const DoctorDashboard = (props) => {
  const [dataa, setData] = useState({
    ID:""
  });
  const [errorMessage, seterrorMessage] = useState(false);
  
  const [AllDataa, setAllDataa] = useState([]);
  

  useEffect(() => {
    getService("ReturnAllAproval", [])
      .then((res) => {
        console.log("res", res?.data?.AllAprovalls);
         setAllDataa(res?.data?.AllAprovalls);
      })
      .catch((error) => {
        console.log("error", error?.response?.data);
      });
  }, []);
 

  const GetApprovalbyID = () => {
    seterrorMessage(false);
    postService("GetApprovalbyID", dataa)
      .then((res) => {
        console.log("res", res?.data?.message?.ID);
        setAllDataa([res?.data?.message]);
      })
      .catch((error) => {
        console.log(error?.response?.data?.[0]);
         seterrorMessage("ID is not found");
         //seterrorMessage(error?.response?.data.message);
      });
    console.log(dataa);
    // setResults(dataa);
    // console.log(results);

  };
  


   
    return (
        <div style={{
            display: "flex",
           justifyContent: "space-between",
        }}>
        
        <div  style={{
            backgroundColor:"#ffff",
            position: "absolute",
             height:"70%",
             width: "70%",
             top: "20%",
             left: "25%",
             

            
        }}>
         <div
         style={{
            flexDirection:"row",
            justifyContent:"space-between",
            display:"flex"
         }}>
          <p style={{
              textTransform:"capitalize",
              fontWeight:"bold",
              fontSize:"20px",
              marginLeft:"20px",
              color:" #286CA5"
          }}>Records</p>
           <Search style={{
              margin: "3%",
              backgroundColor: "#F5F5F5"
          }}>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
              onChange={(ID) => {
                setData({ ...dataa, ID: ID?.target.value });
               }}
            />  
                  
           <button
           style={{
            color:"#fff",
            backgroundColor:"#286CA5",
            marginRight:"10px"
           }}
           onClick={GetApprovalbyID}
           >Search</button>
         
          </Search>
     
          </div>
          {errorMessage && (
          <h2
            style={{
              color: errorMessage.includes("success") ? "red" : "red",
            }}
            style={{
                margin: "0%",
                padding: "0%",
                fontSize: "15px",
                marginTop:"15px",
                color:"red",
                marginLeft:"55%",
                marginTop:"-15px",
            }}
          >
            {errorMessage}
          </h2>
        )}
          <p style={{
            marginLeft: "1%",
            marginTop:"-3%",
            fontSize:"15px"
          }}
          onClick={() => props?.history?.push?.("/PindingApproval")}
          >Pinding all approval</p>
          <DataTable data={AllDataa}/>
        </div>   
        </div>
     
        
    );
}