import React from "react";
import  { Header } from "./header";
import profile from "../../assets/img/profile.png";
import style from "./style.module.css";
import homeIcon2 from "../../assets/img/homeIcon2.png";
import user from "../../assets/img/user.png";
import { Dashboard } from "./dashboard";
import { DoctorDashboard } from "./doctorDashboard";
import { Slider } from "../component/slider"




export const home = (props) => {
  return (
    <div className={style.page}>
    <Header  history={props?.history} />
    <div className={style.div}>
     <Slider history={props?.history}/>
     <DoctorDashboard  history={props?.history}/>
    </div>
    </div>
  );
};
