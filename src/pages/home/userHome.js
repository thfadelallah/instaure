import React from "react";
import  { Header } from "./header";
import profile from "../../assets/img/profile.png";
import style from "./style.module.css";
import { Slider } from "../component/slider";
import { Dashboard } from "./dashboard";





export const userHome = (props) => {
  return (
    <div className={style.page}>
    <Header  history={props?.history} />
    <div className={style.div} >
     <Slider history={props?.history}/>
     <Dashboard  history={props?.history}/>
    </div>
    </div>
  );
};
