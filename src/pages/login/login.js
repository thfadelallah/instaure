import React, { useState, useEffect } from "react";
import style from "../register/style.module.css";
import { TextField, Button } from "@material-ui/core";
import img2 from "../../assets/img/img2.png";
import { postService } from "../services/axios";
import {  setItem } from "../services/storage";
import { useHistory } from "react-router";


export const Login = (props) => {
  const [dataa, setData] = useState({
    email: "",
    pass: "",
  });
  const history = useHistory();
  const [errorMessage, seterrorMessage] = useState(false);
  useEffect(() => {
    console.log("history", history);
    // browserHistory.replace("Home");
  }, []);

  const sendRequseLogin = async () => {
    console.log(dataa);
    postService("Login", dataa)
      .then(async (res) => {
        setItem("userData", res?.data?.account );
        // await setItem("userData", res?.data?.account);
        console.log("res", res?.data);
        props?.history?.push?.("home");
      })
      .catch((error) => {
        console.log("error", error?.response?.data?.error);
        seterrorMessage(error?.response?.data?.error);
      });
  };

  return (
    <div>
      <img src={img2} width="34%" style={{ marginLeft: "5%" }} />
      <h1 className={style.welcome}>welcome </h1>
      <div className={style.input}>
        <TextField
          label="Email"
          type="email"
          variant="outlined"
          onChange={(email) => {
            setData({ ...dataa, email: email?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />
        <TextField
          label="Password"
          type="password"
          autoComplete="current-password"
          variant="outlined"
          onChange={(pass) => {
            setData({ ...dataa, pass: pass?.target.value });
          }}
          style={{ marginTop: "20px", width: "70%" }}
        />

        {errorMessage && (
          <h2 className={style.errorMessage}>{errorMessage}</h2>
        )}
      </div>
      <Button
        style={{
          marginLeft: "50%",
          marginTop: "20px",
          backgroundColor: "#286CA5",
          width: "30%",
          color: "#ffffff",
          //marginBottom: "5%"
        }}
        variant="contained"
        onClick={() => sendRequseLogin()}
      >
        login
      </Button>
    </div>
  );
};
