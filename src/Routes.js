import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { Register } from "./pages/register/register";
import { Login } from "./pages/login/login";
import { home } from "./pages/home/home";
import { Account } from "./pages/account";
import { contact } from "./pages/contact/contact"
import { CreateApproval } from "./pages/createApproval";
import { PindingApproval } from "./pages/PindingApproval";
import {userHome} from "./pages/home/userHome";
import { AddPrice } from "./pages/addPrice";




class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path={"/PindingApproval"} component={PindingApproval} />
          <Route path={"/AddPrice"} component={AddPrice} />
          <Route path={"/userHome"} component={userHome} />
          <Route path={"/PindingApproval"} component={PindingApproval} />
          <Route path={"/CreateApproval"} component={CreateApproval} />
          <Route path={"/contact"} component={contact} />
          <Route path={"/Account"} component={Account} />
          <Route path={"/home"} component={home} />
          <Route path={"/Login"} component={Login} />
          <Route path={"/"} component={Register} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Routes;
